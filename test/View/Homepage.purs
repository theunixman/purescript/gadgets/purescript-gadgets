module App.View.Homepage where

import App.Events (Event)
import App.State (State)
import Control.Bind (discard)
import Data.Function (($))
import Data.Monoid ((<>))
import Text.Smolder.HTML (div, h1, h2, ul, li, a, p)
import Text.Smolder.HTML.Attributes (className, href)
import Text.Smolder.Markup ((!), text)

view :: State -> HTML Event
view s = do
    div ! className "grid-x" $ do
        div ! className "cell" $
            h1 ! className "page-title" $ text "Diagnostic Training System"

    navigation s $
        div ! className "small-offset-1 small-9 medium-9 large-8 cell" $ do
            p $ text ("Welcome to the Diagnostic Training System. "
                      <> "Here, you can update and test our training model.")

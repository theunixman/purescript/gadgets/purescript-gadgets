module App.View.Navigation (navigation) where

import App.Events (Event(..))
import App.Routes (Route(..))
import App.State (State)
import Control.Bind (discard)
import Data.Function (($), const)
import Pux.DOM.HTML (HTML)
import Pux.DOM.Events (onClick)
import Text.Smolder.HTML (div, h2, ul, li, a, nav)
import Text.Smolder.HTML.Attributes (className, href)
import Text.Smolder.Markup ((!), text, (#!))

navigation s c =
    div ! className "grid-x grid-margin-x" $ do
        div ! className "small-2 medium-2 cell" $ do
            nav do
                h2 $ text "Training"
                ul ! className "menu vertical" $ do
                    li $ a ! href "/inputs" #! onClick (const $ PageView Inputs) $ text "Inputs"
                    li $ a ! href "#" $ text "Symptoms"
                    li $ a ! href "#" $ text "Symptom Classes"
                    li $ a ! href "#" $ text "Train"

        div ! className "small-10 medium-10 cell" $ c

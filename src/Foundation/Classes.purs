module Foundation.Classes where

import Prim (kind Symbol, String)

-- | Any HTML @class@ from Foundation.
-- |
-- | This promotes an HTML @class@ to a 'Symbol'.
class FoundationClass (n :: Symbol) c | c -> n

instance foundationClass :: FoundationClass n c

module Gadget.View (
    class View,
    _view,
    view
    ) where

import Data.Function (($))
import Data.Lens (Getter', to)
import FRP.Behavior (Behavior)
import FRP.Event (class IsEvent)
import Text.Smolder.Markup (Markup)

-- | Anything that has at least a 'Markup'.
class View g ev | g -> ev where
    _view :: g -> Markup (Behavior ev)

-- | Evaluate and return the 'Markup' @ev@ for a 'View'.
-- |
-- | @ev@ should be an instance of 'IsEvent'.
view :: forall g ev a. IsEvent ev => View g (ev a) => Getter' g (Markup (Behavior (ev a)))
view = to $ _view

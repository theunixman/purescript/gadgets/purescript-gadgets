const path = require('path');
const webpack = require('webpack');
const isProd = process.env.NODE_ENV === 'production';

const extract = require("extract-text-webpack-plugin");

const entries = [path.join(__dirname, 'test/js/index.js')];

const plugins = [
    new webpack.DefinePlugin({
        'process.env.NODE_ENV': JSON.stringify(process.env.NODE_ENV)
    }),

    new webpack.ProvidePlugin({
        $: 'jquery',
        jQuery: 'jquery',
        jquery: 'jquery'
    }),

    new extract("assets/css/index.css")
];

if (isProd) {
    plugins.push(
        new webpack.LoaderOptionsPlugin({
            minimize: true,
            debug: false
        })
    );
};

module.exports = {
    entry: entries,
    context: __dirname,
    target: 'web',
    output: {
        path: path.join(__dirname, 'site'),
        filename: 'assets/js/index.js',
        publicPath: "/"
    },

    devtool: "source-map",

    module: {

        rules: [
            {
                // Purescript
                test: /\.purs$/,
                exclude: /node_modules/,
                use: {
                    loader: 'purs-loader',
                    options: {
                        psc: 'psa',
                        pscIde: true,
                        pscIdeColors: false,
                        pscPackage: true
                    }
                }
            },

            {
                // Foundation JavaScript
                test: /.*\/(foundation-sites|what-input|motion-ui|js)\/.*\.js$/,
                use:
                {
                    loader: 'babel-loader'
                }
            },

            {
                // SCSS files modules.
                test: /\.scss$/,
                use: extract.extract({
                    fallback: "style-loader",
                    use:
                    [
                        {
                            loader: "css-loader",
                            options: {
                                sourceMap: true
                            }
                        },

                        {
                            loader: "postcss-loader",
                            options: {
                                sourceMap: true
                            }
                        },

                        {
                            loader: "resolve-url-loader",
                            options: {
                                sourceMap: true
                            }
                        },

                        {
                            loader: "sass-loader",
                            options: {
                                sourceMap: true
                            }
                        },
                    ]
                })
            },

            {
                // fonts, which should be in a folder called fonts because of svg.
                test: /\/fonts\/.*\.(otf|eot|svg|ttf|woff(2?))/,
                use: {
                    loader: "file-loader",
                    options: {
                        outputPath: 'assets/fonts/',
                        name: '[name].[ext]'
                    }
                }
            },

            {
                // image files
                test: /images\/.*\.(png|jpg|svg|gif)/,
                use: {
                    loader: "file-loader",
                    options: {
                        outputPath: 'assets/images/',
                        name: '[name].[ext]'
                    }
                }
            },

            {
                // favicon
                test: /\.(ico)$/,
                use: {
                    loader: "file-loader",
                    options: {
                        name: '[name].[ext]'
                    }
                }
            },

            {
                // html files
                test: /\.(html)$/,
                use: [
                    {
                        loader: "file-loader",
                        options: {
                            name: '[name].[ext]'
                        }
                    },

                    {
                        loader: "extract-loader"
                    },

                    {
                        loader: "html-loader",
                        options: {
                            root: path.resolve(__dirname, 'test')
                        }
                    }
                ]
            }
        ]
    },

    plugins: plugins,

    resolveLoader: {
        modules: [
            path.join(__dirname, 'node_modules')
        ]
    },

    resolve: {

        alias: {
            'jquery': 'jquery/src/jquery'
        },

        modules: [
            path.join(__dirname, 'node_modules')
        ],

        extensions: ['.js', '.purs', '.scss']
    },

    performance: { hints: false },

    stats: {
        hash: false,
        timings: false,
        version: false,
        assets: false,
        errors: true,
        colors: false,
        chunks: false,
        children: false,
        cached: false,
        modules: false,
        chunkModules: false
    }
}

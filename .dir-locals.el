(
 (purescript-mode . ((projectile-project-compilation-cmd . "psc-package build"))))
 (nil . (
         (projectile-project-compilation-cmd . "npm run build")
         (projectile-project-compilation-dir . "/")
         )
      )
 )

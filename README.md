<!-- markdown-toc start - Don't edit this section. Run M-x markdown-toc-refresh-toc -->
**Table of Contents**

- [Foundation Behaviors](#foundation-behaviors)
- [Overview](#overview)
    - [Building](#building)
    - [Project Structure](#project-structure)
        - [Gadgets](#gadgets)

<!-- markdown-toc end -->

# Foundation Behaviors

# Overview

This is a web-application service based on combining [behaviors][behaviors],
[smolder][smolder], [FontAwesome][fa], and [Foundation][foundation].

[behaviors]: https://pursuit.purescript.org/packages/purescript-behaviors/
[smolder]: https://pursuit.purescript.org/packages/purescript-smolder/
[fa]: http://fontawesome.io/
[foundation]: http://foundation.zurb.com/sites/docs/

## Building

To build, first ensure you have [`psc-package`][psc-package],
[PureScript][psc], and [yarn][yarn] installed.

[psc-package]: https://github.com/purescript/psc-package/#psc-package
[purescript]: http://www.purescript.org/
[yarn]: https://yarnpkg.com/lang/en/

After the first checkout, or pulling changes from our repository, run:

    yarn install
    psc-package build

Then, as you make changes, you only need run:

    psc-package build

As soon as it’s published on [Pursuit](https://pursuit.purescript.org) you’ll be able to add
`purescript-foundation` as a package in your `psc-package.json` for your
own projects. Until then, use [our package set](https://gitlab.com/theunixman/purescript/psc-package-sets/).

## Project Structure

### Gadgets

_Gadgets_ are vertical slices of [Smolder Markup][smo-markup], an arbitrary state, and
a [Behavior][be-behavior]. Much of the provided _Gadgets_ wrap [Foundation][foundation] and
[FontAwesome][fa] components. The components that emit [Events][fnd-events] provide
adapters to the [][][`IsEvent class`][be-isevent].

[smo-markup]: https://pursuit.purescript.org/packages/purescript-smolder/10.2.0/docs/Text.Smolder.Markup
[be-behavior]: https://github.com/paf31/purescript-behaviors/blob/v6.0.0/generated-docs/FRP/Behavior.md
[be-isevent]: https://github.com/paf31/purescript-behaviors/blob/v6.0.0/generated-docs/FRP/Event/Class.md
